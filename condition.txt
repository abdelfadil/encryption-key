CONDITION BENERALE D'UTILISATION 
Veuillez lire attentivement les presentes conditions d'utilisation avant d'utiliser ce logiciel

1. QUI SOMMES-NOUS ET COMMENT NOUS CONTECTER 
Computer Security Scan est un logiciel developper par le groupe 4 du cours de Mr Minka de la promotions 2025 des Humanites numeriques. Nous sommes une organisation a responsabilite limitee.
Pour nous contacter, veuillez envoyer un email a info@css.cmr ou appeler notre service client au +237 659 09 30 22.
En utilisant notre logiciel, vous confirmez que vous acceptez ces conditions d'utilisation et vous vous engagez a les respecter. Si vous n'acceptez pas ces conditions, vous ne devez pas utiliser notre logiciel.

2. VOS DONNEES PERSONNELLES
Ces conditions d'utilisation se referent aux termes supplementaires suivants, qui s'appliquent egalement a votre utilisation de notre logiciel:
2.1 Notre politique de confidentialite, qui enonce les conditions selon lesquelles nous traitons les donnees personnelles que nous recueillons aupres de vous. En utilisant notre site, vous consentez a ce traitement et garantissez que toutes les donnees fournies par vous sont exactes.
2.2 Notre politique sur les donnees, qui fournit des informations sur les donnees utilisees sur notre site.

3. A PROPOS DE CETTE CONDITION D'UTILISATION
Nous pouvons modifier ces conditions de temps a autre. Chaque fois que vous souhaitez utiliser notre logiciel, nous vous invitons a consulter ces conditions pour vous assurer de connaitre celles alors en vigueur. La date de la derniere mise a jour de ces conditions est le 27 mars 2023.
Notre logiciel peut etre modifie de temps a autre pour refleter les changements apportes a nos produits, aux besoins de nos utilisateurs et a nos priorites commerciales.
Notre logiciel est disponible gratuitement et peut etre suspendu ou retire a tout moment. Nous ne garantissons pas que notre logiciel, ou tout contenu figurant dans celui-ci, sera toujours disponible ou ininterrompu. Nous pouvons suspendre, retirer ou restreindre la disponibilite de tout ou partie de notre site pour des raisons commerciales et operationnelles. Nous mettons en oeuvre tous les moyens pour vous informer avec un preavis raisonnable de toute suspension ou de tout retrait.
Vous etes egalement responsable de veiller a ce que toutes les personnes qui accedent a notre logiciel par l'intermediaire de votre ordinateur sont au courant des presentes conditions d'utilisation ainsi que des autres modalites applicables, et qu'elles s'y conforment.
Nous vous recommandons d'imprimer une copie de ces conditions pour vous y referer ulterieurement en cas d'interruption.
Bien que nous fassions des efforts raisonnables pour mettre a jour les informations de notre logiciel, nous ne fournissons aucune declaration ou garantie expresse ou implicite quant a l'exactitude, a l'exhaustivite ou a l'actualite du contenu et fonctionnalites de notre logiciel.

4. PROPRIETES
Nous sommes le proprietaire ou le titulaire de tous les droits de propriete intellectuelle sur notre logiciel et des fonctionnalites de celui-ci. Ces elements publies sont proteges par des droits d'auteur, des droits de marques, signes distinctifs ainsi que d'autres lois et traites dans le monde entier. Tous ces droits sont reserves.
Vous vous engagez a utiliser des fonctionnalites du logiciel dans un cadre strictement prive. Une utilisation des contenus a des fines commerciales est strictement interdite.
Notre statut en tant qu'auteurs de documents (et celui de tous les contributeurs identifies) doit toujours etre reconnu.

5. RESPOSABILITES
Nous avons mis en oeuvre tous les moyens necessaires afin de garantir au mieux la securite et la confidentialite des donnees.
Les fonctionnalites de notre logiciel sont reputees fiables. Toutefois, elles sont a titre purement informatif et vous assumez seul l'entiere responsabilite de l'utilisation des informations et contenus du present.
Notre responsabilite ne peut etre engagee en cas de force majeure ou du fait imprevisible et insurmontable d'un tiers.

6. DROIT APPLICABLE ET JURIDICTION COMPETENTE
Si vous etes un consommateur, veuillez noter que ces conditions d'utilisation, leur objet et leur execution sont regis par le droit Camerounais. Vous et nous convenons que les tribunaux du Cameroun auront une juridiction exclusive.
Si vous etes une entreprise, les presentes conditions d'utilisation, leur objet et leur application (et tout differend ou reclamation non contractuel) sont regis par le droit anglais. Nous reconnaissons tous deux la competence exclusive des tribunaux d'Angleterre.